FROM ruby:3.2.0
MAINTAINER Alex Drummer <drummerroma@gmail.com>

ENV BUILD_PACKAGES="curl" \
    DEV_PACKAGES="tzdata libxml2 libxml2-dev default-mysql-client \
                   git nodejs bash syslog-ng yarn"

ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true
ENV SECRET_KEY_BASE 'notverysecret'

ENV INSTALL_PATH /home/app/webapp

RUN apt update && apt upgrade -y  $BUILD_PACKAGES $DEV_PACKAGES
# install native gems
#RUN gem install libv8
RUN gem install nokogiri
RUN gem install mysql2

RUN mkdir -p $INSTALL_PATH
WORKDIR $INSTALL_PATH

COPY Gemfile Gemfile.lock ./

RUN gem install -N bundler

#RUN bundle install    # resolve dependencies for platform-specific gems

RUN bundle config set without 'development test'
RUN bundle install --jobs 20 --retry 5

COPY . ./

# Precompile Rails assets.
# RUN bundle exec rake assets:precompile SECRET_KEY_BASE=${SECRET_KEY_BASE} DB_ADAPTER=nulldb

VOLUME ["$INSTALL_PATH/public"]

CMD ["rails","server","-b","0.0.0.0"]
