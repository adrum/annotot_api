## annotot_api

## README

MAINTAINER Alex Drummer <drummerroma@gmail.com>

Need to persist annotations quick and easily? Annotot, the original mini annotation API is for you. Don't annotate, annotot instead.
This Ruby on Rails Application bundles the annotot gem with a connection to a mysql-Database. The container exposes port 3000 and should run behind a proxy server.
You may run this container in your development environment, connecting directly to port 3000.

This Annotot implementation mounts itself at /. All API endpoints are relative to its mount location.

Find endpoint documentation here: https://github.com/PenguinParadigm/annotot#api


-------------------------------------
## Production

- clone this repository in your production environment

### Build the container:


```bash
docker-compose build
```

### Run the container:

```bash
docker-compose up -d
```

### first run: Migrations

```bash
docker exec -ti app-annotot rails db:migrate
```

### Database backup

```bash
docker exec db_annotot mariadb-dump db_annotot -u{your username} -p"{your password}" > db_annotot.sql
```

Consider to shedule this with a cron-job or integrate something like https://hub.docker.com/r/databack/mysql-backup in your docker-compose script.


### Environment variables

This image uses several environment variables to define different values to be used at run time:

* Variable name: `MYSQL_ROOT_PASSWORD`
* Optional
* Accepted values: Any string.
* Description: Mysql-Database root user password.

----

* Variable name: `MYSQL_DATABASE`
* Accepted values: Any string.
* Description: Mysql Database Name.

----

* Variable name: `MYSQL_USER`
* Accepted values: Any string.
* Description: Mysql Username.

----

* Variable name: `MYSQL_PASSWORD`
* Accepted values: Any string.
* Description: Mysql Password for user MYSQL_USER.

----

* Variable name: `RACK_ENV`
* Accepted values: "development", "production".
* Description: Environment in which the Rails-Application will run.

----

* Variable name: `RAILS_ENV`
* Accepted values: "development", "production".
* Description: Environment in which the Rails-Application will run. Must be the same as above.

----

* Variable name: `SECRET_KEY_BASE`
* Accepted values: Random hash
* Description: To generate a new secret_key_base, use rake secret command.

----

* Variable name: `DB_USERNAME`
* Accepted values: Any string. Must be the same as MYSQL_USER.
* Description: Username used by Rails to connect to the database.

----

* Variable name: `DB_PASSWORD`
* Accepted values: Any string. Must be the same as MYSQL_PASSWORD.
* Description: Password used by Rails to connect to the database.

----

* Variable name: `DB_HOST`
* Accepted values: Any string. Should be the name of the connected mysql container.
* Description: Hostname of the Mysql-Database.

----

* Variable name: `DB_NAME`
* Accepted values: Any string. Should be the name of the mysql database.
* Description: Table name of the Mysql-Database.

----------------------------

## local development

- clone this repository in your local environment

### Build the container:

```bash
docker-compose -f docker-compose-local.yml build
```

### Run the container:

```bash
docker-compose -f docker-compose-local.yml up -d
```

### first run: Migrations

```bash
docker exec -ti app-annotot rails db:migrate
```

### Database export

```bash
docker exec db_annotot mariadb-dump db_annotot -udeveloper -p"developer" > db_annotot.sql
```


